<?php

/**
 * Base controller with legacy output format.
 * 
 * @copyright 2014 FreshRealm
 * @author Ondrej Nebesky <ondrej@freshrealm.co>
 * @author John Grogg <grogg@freshrealm.co>
 * @version 0.91
 * @package FreshRest
 */
class FrApiBaseController1 extends FrApiBaseController {
    
    /**
     * Render data in standartized format using JSON.
     * data is usually associative array for single object
     * or array of arrays.
     * 
     * @param array $data
     * @param integer $code HTTP error code
     * @throws Exception when using a format, which is not implemented yet (jsonp, xml)
     */
    protected function renderOutput($data, $code = 200) {
        // add headers and necessary fields to the output array
        if (isset($this->module->errorCodes[$code])) {
            $message = $this->module->errorCodes[$code];
        } else {
            $message = "N/A";
        }
        $statusString = $code . ' ' . $message;
        header($_SERVER['SERVER_PROTOCOL'] . ' ' . $statusString, true, $code);

        $output = array(
            'code' => $code,
            'timestamp' => time(),
            'data' => $data);

        // send the output out
        if ($this->module->format == 'json') {
            header('content-type: application/json; charset=utf-8');
            echo CJSON::encode($output);
        } else if ($this->module->format == 'jsonp') {
            throw new Exception("not implemented yet", 500);
        } else if ($this->module->format == 'xml') {
            throw new Exception("not implemented yet", 500);
        }
        Yii::app()->end();
    }
}
